from django.urls import path
from core import views

app_name="core"
urlpatterns = [

    ##urls estadio 
    path('lista/estadio/',views.listaestadios.as_view(), name="listaestadio"),
    path('crear/estadio/',views.Createestadio.as_view(), name="crearestadio"),
    path('recuperar/estadio/<int:pk>/', views.Detailestadio.as_view(), name= "recestadio"),
    path('actualizar/estadio/<int:pk>/', views.Updateestadio.as_view(), name= "actuestadio"),
    path('borrar/estadio/<int:pk>/', views.Deleteestadio.as_view(), name= "delestadio"),

    ##urls equipo
    path('lista/equipo/',views.Listaequipos.as_view(), name="listaequipo"),
    path('crear/equipo/',views.Createequipos.as_view(), name="crearequipo"),
    path('recuperar/equipo/<int:pk>/', views.Detailequipos.as_view(), name= "recequipo"),
    path('actualizar/equipo/<int:pk>/', views.Updateequipos.as_view(), name= "actuequipo"),
    path('borrar/equipo/<int:pk>/', views.Deleteequipos.as_view(), name= "delequipo"),

    ##urls ciudad
    path('lista/ciudad/',views.listaciudades.as_view(), name="listaciudad"),
    path('crear/ciudad/',views.Createcuidad.as_view(), name="crearciudad"),
    path('recuperar/ciudad/<int:pk>/', views.Detailciudad.as_view(), name= "recuciudad"),
    path('actualizar/ciudad/<int:pk>/', views.Updateciudad.as_view(), name= "actuciudad"),
    path('borrar/ciudad/<int:pk>/', views.Deleteciudad.as_view(), name= "delciudad"),
]