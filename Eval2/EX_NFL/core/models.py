from django.db import models

# Create your models here.

class Ciudades(models.Model):
    Nombre = models.CharField(max_length=35, default= "Ciudad")
    def __str__(self):
        return self.Nombre

class Equipos(models.Model):
    Nombre = models.CharField(max_length=35, default= "Tuequipo")
    Ciudad = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    def __str__(self):
        return self.Nombre

class Estadios(models.Model):
    Nombre = models.CharField(max_length=35)
    Equipos= models.ForeignKey(Equipos, on_delete=models.CASCADE)
    def __str__(self):
        return self.Nombre

