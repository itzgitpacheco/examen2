from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *



# Create your views here.

##STADIOS

class listaestadios(generic.View):
    template_name = "core/estadio/listaestadio.html"
    context = {}

    def get(self, request, *args, **kwargs):
        Estadio = Estadios.objects.all()
        self.context = {
            "Estadios": Estadio
        }
        return render(request, self.template_name,self.context)


class Detailestadio(generic.DetailView):
    template_name = "core/estadio/recestadio.html"
    model = Estadios


class Createestadio(generic.CreateView):
    template_name = "core/estadio/crearestadio.html"
    model = Estadios
    form_class = estadiosforms
    success_url = reverse_lazy("core:listaestadio")


class Updateestadio(generic.UpdateView):
    template_name = "core/estadio/actuestadio.html"
    model = Estadios
    form_class = actulizarestadiosforms
    success_url = reverse_lazy("core:listaestadio")

class Deleteestadio(generic.DeleteView):
    template_name = "core/estadio/delestadio.html"
    model = Estadios
    success_url = reverse_lazy("core:listaestadio")


#Equipos

class Listaequipos(generic.View):
    template_name = "core/equipo/listaequipo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipo =  Equipos.objects.all()
        self.context = {
            "Equipos": equipo
        }
        return render(request, self.template_name,self.context)
    
class Createequipos(generic.CreateView):
    template_name = "core/equipo/crearequipo.html"
    model = Equipos
    form_class = equipoforms
    success_url = reverse_lazy("core:listaequipo")

class Detailequipos(generic.DetailView):
    template_name = "core/equipo/recequipo.html"
    model = Equipos

class Updateequipos(generic.UpdateView):
    template_name = "core/equipo/actuequipo.html"
    model = Equipos
    form_class = actualizarequiposforms
    success_url = reverse_lazy("core:listaequipo")

class Deleteequipos(generic.DeleteView):
    template_name = "core/equipo/delequipo.html"
    model = Equipos
    success_url = reverse_lazy("core:listaequipo")

#Ciudad 

class listaciudades(generic.View):
    template_name = "core/ciudad/listaciudad.html"
    context = {}

    def get(self, request, *args, **kwargs):
        Ciudad = Ciudades.objects.all()
        self.context = {
            "Ciudades": Ciudad
        }
        return render(request, self.template_name,self.context)

class Createcuidad(generic.CreateView):
    template_name = "core/ciudad/crearciudad.html"
    model = Ciudades
    form_class = ciudadesforms
    success_url = reverse_lazy("core:listaciudad")

class Detailciudad(generic.DetailView):
    template_name = "core/ciudad/recciudad.html"
    model = Ciudades

class Updateciudad(generic.UpdateView):
    template_name = "core/ciudad/actciudad.html"
    model = Ciudades
    form_class = actualizarciudadesforms
    success_url = reverse_lazy("core:listaciudad")

class Deleteciudad(generic.DeleteView):
    template_name = "core/ciudad/delciudad.html"
    model = Ciudades
    success_url = reverse_lazy("core:listaciudad")