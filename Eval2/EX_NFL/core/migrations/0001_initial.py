# Generated by Django 4.2.7 on 2023-11-05 11:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ciudades',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Houston', max_length=16)),
            ],
        ),
        migrations.CreateModel(
            name='Equipos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Beers', max_length=16)),
                ('Ciudad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.ciudades')),
            ],
        ),
        migrations.CreateModel(
            name='Estadios',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(default='Raymond James Stadium', max_length=16)),
                ('Equipos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.equipos')),
            ],
        ),
    ]
