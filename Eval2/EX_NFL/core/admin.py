from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(Estadios)
class estAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Equipos",
    ]

@admin.register(Equipos)
class EqupAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
        "Ciudad",
    ]

@admin.register(Ciudades)
class CiudadAdmin(admin.ModelAdmin):
    list_display = [
        "Nombre",
    ]