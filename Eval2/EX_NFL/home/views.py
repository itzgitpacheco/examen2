from django.shortcuts import render
from django.views import generic

# Create your views here.
class inicio(generic.View):
    template_name = "home/inicio.html"
    context={}
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.context)